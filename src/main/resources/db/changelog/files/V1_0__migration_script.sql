CREATE TABLE public.patient
(
    id SERIAL,
    creation_date timestamp without time zone,
    full_name character varying(255) COLLATE pg_catalog."default",
    is_deleted boolean DEFAULT false,
    CONSTRAINT patient_pkey PRIMARY KEY (id)
);

CREATE TABLE public.appointment
(
    id SERIAL,
    cancellation_reason text COLLATE pg_catalog."default",
    date_from timestamp without time zone,
    date_to timestamp without time zone,
    is_canceled boolean DEFAULT false,
    patient_id bigint,
    CONSTRAINT appointment_pkey PRIMARY KEY (id),
    CONSTRAINT fk4apif2ewfyf14077ichee8g06 FOREIGN KEY (patient_id)
        REFERENCES public.patient (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

INSERT INTO public.patient(creation_date, full_name, is_deleted)
	VALUES 
	(NOW(), 'test1', false),
	(NOW(), 'test2', false),
	(NOW(), 'test3', false),
	(NOW(), 'test4', false);
	
	INSERT INTO public.appointment(cancellation_reason, date_from, date_to, is_canceled, patient_id)
	VALUES 
	(null, now(), now(), false, 1),
	('test-1', now(), now(), false, 1),
	(null, now(), now(), false, 2),
	('test-2', now(), now(), false, 2),
	(null, now(), now(), false, 3),
	('test-3', now(), now(), false, 3),
	(null, now(), now(), false, 4),
	('test-4', now(), now(), false, 4);