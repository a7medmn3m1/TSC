package com.stc.clinic.assignment.modules.posts.model;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "PostComment")
@Table(name = "post_comment")
@Data
public class PostComment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
 
    private String review;
 
    @ManyToOne
    private Post post;


}
