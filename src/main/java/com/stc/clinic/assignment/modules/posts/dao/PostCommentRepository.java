package com.stc.clinic.assignment.modules.posts.dao;

import com.stc.clinic.assignment.modules.posts.model.Post;
import com.stc.clinic.assignment.modules.posts.model.PostComment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostCommentRepository extends JpaRepository<PostComment, Long> {
}
