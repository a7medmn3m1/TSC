package com.stc.clinic.assignment.modules.posts.controller;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.stc.clinic.assignment.modules.posts.model.Post;
import com.stc.clinic.assignment.modules.posts.model.PostComment;
import com.stc.clinic.assignment.modules.posts.model.PostDetails;
import com.stc.clinic.assignment.modules.posts.service.PostCommentService;
import com.stc.clinic.assignment.modules.posts.service.PostDetailsService;
import com.stc.clinic.assignment.modules.posts.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostController implements Serializable{

	private static final long serialVersionUID = 1655566133996779096L;
    
    @Autowired
    private PostService postService;
	@Autowired
	private PostCommentService postCommentService;
	@Autowired
	private PostDetailsService postDetailsService;

	@PostMapping("/post")
	public ResponseEntity<?> save(HttpServletRequest request, @RequestBody Post post)  {
		try {
			postService.save(post);
			return ResponseEntity.ok().body(HttpStatus.OK);
		} catch (Exception e) {
			
			e.printStackTrace();
			return  new ResponseEntity<>("save new post Failed", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/comment")
	public ResponseEntity<?> saveComment(HttpServletRequest request, @RequestBody List<PostComment> comments)  {
		try {
			postCommentService.saveComment(comments);
			return ResponseEntity.ok().body(HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return  new ResponseEntity<>("save new post Failed", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/details")
	public ResponseEntity<?> saveDetails(HttpServletRequest request, @RequestBody PostDetails details)  {
		try {
			postDetailsService.saveDetails(details);
			return ResponseEntity.ok().body(HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return  new ResponseEntity<>("save new post Failed", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
