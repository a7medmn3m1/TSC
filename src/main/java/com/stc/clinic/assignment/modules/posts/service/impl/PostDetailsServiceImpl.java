package com.stc.clinic.assignment.modules.posts.service.impl;

import com.stc.clinic.assignment.modules.posts.dao.PostDetailsRepository;
import com.stc.clinic.assignment.modules.posts.model.PostDetails;
import org.springframework.stereotype.Service;

import com.stc.clinic.assignment.modules.posts.service.PostDetailsService;

@Service("postDetailsService")
public class PostDetailsServiceImpl implements PostDetailsService {

    private PostDetailsRepository postDetailsRepository;

    @Override
    public void saveDetails(PostDetails details) {
        postDetailsRepository.save(details);
    }
}
