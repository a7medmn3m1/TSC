package com.stc.clinic.assignment.modules.posts.service;

import com.stc.clinic.assignment.modules.posts.model.Post;
import com.stc.clinic.assignment.modules.posts.model.PostComment;

import java.util.List;

public interface PostService {

    void save(Post post);

}
