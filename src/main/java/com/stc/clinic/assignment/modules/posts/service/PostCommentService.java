package com.stc.clinic.assignment.modules.posts.service;

import com.stc.clinic.assignment.modules.posts.model.PostComment;

import java.util.List;

public interface PostCommentService {

    void saveComment(List<PostComment> comments);
}
