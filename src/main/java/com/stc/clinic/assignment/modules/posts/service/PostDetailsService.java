package com.stc.clinic.assignment.modules.posts.service;

import com.stc.clinic.assignment.modules.posts.model.PostDetails;

public interface PostDetailsService {

    void saveDetails(PostDetails details);
}
