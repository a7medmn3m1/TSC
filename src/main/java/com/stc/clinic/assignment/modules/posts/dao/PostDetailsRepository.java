package com.stc.clinic.assignment.modules.posts.dao;

import com.stc.clinic.assignment.modules.posts.model.Post;
import com.stc.clinic.assignment.modules.posts.model.PostDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostDetailsRepository extends JpaRepository<PostDetails, Long> {
}
