package com.stc.clinic.assignment.modules.posts.model;

import lombok.Data;

import java.util.Date;

import javax.persistence.*;

@Entity(name = "PostDetails")
@Table(name = "post_details")
@Data
public class PostDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
 
    @Column(name = "created_on")
    private Date createdOn;
 
    @Column(name = "created_by")
    private String createdBy;
 
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JoinColumn(name = "id")
    private Post post;



}
