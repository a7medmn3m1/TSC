package com.stc.clinic.assignment.modules.posts.service.impl;

import com.stc.clinic.assignment.modules.posts.dao.PostCommentRepository;
import com.stc.clinic.assignment.modules.posts.model.PostComment;
import org.springframework.stereotype.Service;

import com.stc.clinic.assignment.modules.posts.service.PostCommentService;

import java.util.List;

@Service("postCommentService")
public class PostCommentServiceImpl implements PostCommentService {

    private PostCommentRepository postCommentRepository;

    @Override
    public void saveComment(List<PostComment> comments) {
        postCommentRepository.saveAll(comments);

    }
}
