package com.stc.clinic.assignment.modules.posts.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stc.clinic.assignment.modules.posts.dao.PostRepository;
import com.stc.clinic.assignment.modules.posts.model.Post;
import com.stc.clinic.assignment.modules.posts.service.PostService;

@Service("postService")
public class PostServiceImpl implements PostService {

    @Autowired
    PostRepository postRepository;

    @Override
    public void save(Post post) {
    	postRepository.save(post);
    }
}
