package com.stc.clinic.assignment.modules.posts.dao;

import com.stc.clinic.assignment.modules.posts.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {
}
